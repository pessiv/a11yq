1. what is accessibility?
Accessibility is the practice of making information, activities, and/or environments sensible, meaningful, and usable for as many people as possible.

1.1. how do we make information usable for as many people as possible?
To make information usable for as many people as possible, wcag should be consulted.

1.1.1. what is in wcag?
wcag 2
wcag 3

1.1.1.1. what is in wcag 2?
1.1.1.2. what is in wcag 3?

1.1.2. who created wcag?
WCAG 2.0 refers to Web Content Accessibility Guidelines, which are published by the World Wide Web Consortium's (W3C) Web Accessibility Initiative (WAI)

1.1.2.1. what is the definition of a guideline?
1.1.2.2. how does w3c operate?
1.1.2.3. how does wai operate?

1.1.3. are there any other standards that should be used?
atag
uaag
wai-aria
act & earl

1.1.3.1. what is atag?
1.1.3.2. what is uaag?
1.1.3.3. what is wai-aria?
1.1.3.4. what is act & earl?

1.2. how do we make activities usable for as many people as possible?
If you want to practice inclusive design, you MUST: work with people with disabilities to explore the problems you're trying to solve.

1.2.1. what is inclusive design?
Summary: Inclusive design describes methodologies to create products that understand and enable people of all backgrounds and abilities. It may address accessibility, age, economic situation, geographic location, language, race, and more.

1.2.1.1. what can poor disabled people do?
1.2.1.2. how bad can a language barrier be?
1.2.1.3. how bad can racism get?
1.2.1.4. how bad is agism?

1.2.2. where can i learn inclusive design?
books:
Accessibility for everyone - Laura Kalbag
A Web for Everyone by Sarah Horton and Whitney Quessenbery
Design for Real Life by Eric Meyer
Just Ask: integrating accessibility through design
Articles:
Designing for Accessibility is not that hard
Designing for accessibility and inclusion
usability testing with people on the autism spectrum: what to expect
the audience you didn't know you had
think like an accessible ux researcher part 3: five common mistakes in usability tesitng and how to avoid them
color accessibility : Tools and resources to help you design inclusive products
How to advocate for accessible and inclusive tech events
Podcasts:
A11y Rules
Email lists:
A11y weekly
Other resources:
inclusive: a microsoft toolkit - manual (pdf)
inclusive: a microsoft toolkit - activities (pdf)
microsoft website resources

1.2.2.1. insert questions for each resource here

1.2.3. where can I find people with disabilities?
discord servers and other internet servers

1.2.3.1. what are some examples of them?

1.3. how do we make environments usable for as many people as possible?
Accessible design is a design process in which the needs of people with disabilities are specifically considered. Accessibility sometimes refers to the characteristic that products, services, and facilities can be independently used by people with a variety of disabilities. Accessibility as a design concern has a long history, but public awareness about accessibility increased with the passage of legislation such as the Americans with Disabilities Act (ADA), which mandated that public facilities and services be fully accessible to people with disabilities.

1.3.1. where can I learn accessible design?
https://www.w3.org/WAI/fundamentals/foundations-course/ and some other courses

1.3.2. who runs ADA?
The U.S. Department of Justice enforces ADA regulations governing state and local government services (Title II) and public accommodations (Title III).

1.3.2.1. what does The U.S. Department of Justice do?
1.3.2.2. what is title II?
1.3.2.3. what is title III?

1.3.3. who makes the most accessible designs?
//TODO: answer this later.

1.3.4. when did accessibility design begin?
In 1961, the American National Standard Institute published its first standard for accessible design. Over the next several decades, state and federal legislation put these standards into law. In 1990, the Americans with Disabilities Act (ADA) set out a clear prohibition of discrimination based on disability.

1.3.4.1. what is the American National Standard Institute?
1.3.4.2. what are the standards for accessible design?
1.3.4.3. what are the laws for accessible design?

2. why do we need more accessibility?
Improving accessibility brings about increased quality of life; creates more independence and better social integration. It also leads to better health and can result in cost saving in a number of areas.  In fact improved accessibility gives people with disabilities the opportunity for independent living within the community thus leading to lower demands for special services therefore bringing economic benefits to all members of society.

2.1. what is the most common type of disability?
    Attention Deficit Hyperactivity Disorder (ADHD)
    Learning Disabilities
    Mobility Disabilities
    Medical Disabilities
    Psychiatric Disabilities
    Traumatic Brain Injury (TBI) and Post-Traumatic Stress Disorder (PTSD)
    Visual Impairments
    Deaf and Hard of Hearing
    Concussion
    Autism Spectrum Disorders

2.1.1. what are the symptoms of adhd?
The 3 categories of symptoms of ADHD include the following:

    Inattention: Short attention span for age (difficulty sustaining attention) Difficulty listening to others. ...
    Impulsivity: Often interrupts others. ...
    Hyperactivity: Seems to be in constant motion; runs or climbs, at times with no apparent goal except motion.

2.1.2. what are the most common learning disabilities?
Keep reading to find out the 5 most common learning disabilities special education and their symptoms.

    Dyslexia. Dyslexia is probably the number one learning disorder auditory processing, visual processing disorders may have trouble that affects children and adults. ...
    ADHD. ...
    Dyscalculia. ...
    Dysgraphia. ...
    Dyspraxia.

2.1.3. what are the most common mobility disabilities?
These include but are not limited to amputation, paralysis, cerebral palsy, stroke, multiple sclerosis, muscular dystrophy, arthritis, and spinal cord injury.

2.1.4. what are the most common medical disabilities?
10 of the most common conditions that qualify for disability are Arthritis, Heart Disease, Degenerative Disc Disease, Respiratory Illness, Mental Illness, Cancer, Stroke, Nervous System Disorders, Diabetes, Immune System Disorders.

2.1.5. what are the most common psychiatric disabilities?
Common mental health problems

    Depression.
    Generalised anxiety disorder.
    Panic disorder.
    Obsessive-compulsive disorder.
    Post-traumatic stress disorder.

2.1.6. what are the symptoms of tbi?
Physical symptoms

    Loss of consciousness from several minutes to hours.
    Persistent headache or headache that worsens.
    Repeated vomiting or nausea.
    Convulsions or seizures.
    Dilation of one or both pupils of the eyes.
    Clear fluids draining from the nose or ears.
    Inability to awaken from sleep.

2.1.7. what are the symptoms of ptsd?
People with PTSD have intense, disturbing thoughts and feelings related to their experience that last long after the traumatic event has ended. They may relive the event through flashbacks or nightmares; they may feel sadness, fear or anger; and they may feel detached or estranged from other people.

2.1.8. what are the most common visual impairments?
The most common causes of visual impairment globally are uncorrected refractive errors (43%), cataracts (33%), and glaucoma (2%). Refractive errors include near-sightedness, far-sightedness, presbyopia, and astigmatism. Cataracts are the most common cause of blindness.

2.1.9. what are the symptoms of concussion?
    Headache or “pressure” in head.
    Nausea or vomiting.
    Balance problems or dizziness, or double or blurry vision.
    Bothered by light or noise.
    Feeling sluggish, hazy, foggy, or groggy.
    Confusion, or concentration or memory problems.
    Just not “feeling right,” or “feeling down”.

2.1.10. what are the most common autism spectrum disorders?
The three most common forms of autism in the pre-2013 classification system were Autistic Disorder—or classic autism; Asperger's Syndrome; and Pervasive Developmental Disorder – Not Otherwise Specified (PDD-NOS). These three disorders share many of the same symptoms, but they differ in their severity and impact.

2.2. what is the definition of independent living?
Independent Living means being as self-sufficient as possible. It means taking risks and being allowed to succeed and fail on your own terms. Independent Living means being able to exercise the greatest degree of choice about where you live, with whom you live, how you live, where you work, and how you use your time.

2.2.1. how to exercise the greatest degree of choice about where you live?
2.2.2. -||- whom you live?
2.2.3. -||- how you live?
2.2.4. -||- where you work?
2.2.5. -||- how you use your time?

2.3. what are special services for people with disabilities?
Services for people with severe disabilities include:
    transport services.
    home modifications.
    home equipment and devices.
    personal assistance for leisure outside the home.
    adaptation training.
    support signs.

2.3.1. what are the different transport services?
Buses. Many rural communities use buses as the primary vehicle for their public transportation systems, operating fixed-route service on a regular schedule. ...
Passenger Train Service. ...
Passenger Air Service. ...
Personal Vehicles. ...
Pedestrian Transportation. ...
Boats. ... 

2.3.2. what are the home modifications?
Accessible Home Modifications for People with Disabilities

    Handicap Ramps.
    Door and Hallway Widening.
    Accessible Bathrooms.
    Accessible Bedrooms.
    Automatic Door Openers.
    Low-Pile Carpet and Smooth Flooring.
    Stairlifts and Porch Lifts.
    Ceiling Track Lifts.

2.3.3. what are the home equipments and devices for the disabled?
Elastic stockings
appliances for colostomies
some type of trusses or wound dressings
urinary catheters
pressure relieving cushions and mattresses
continence pads
wheelchairs and walking aids
hearing and vision aids
artificial limbs and surgical appliances
communication aids

2.3.4. what are the different types of adaptation training?
2.3.5. what are the different support signs?

3. what tech exists to create it already?
types of assistive technology:
vision
hearing
speech communication
learning, cognition and developmental
mobility, seating and positioning
daily living
environmental adaptations
vehicle modification and transportation
computers and related peripherals

3.1. what technology is in vision tech?
large print books
typoscope
reading stands
low vision lamps
optical magnifiers
electronic magnification aids
braille reading materials
refreshable braille display
braille translator software
audio format materials
screen readers software
braille slate and stylus
jot a dot
braille typewriter
braille computer keyboards
large computer keyboards
digital audio recorder
braille electronic note taker

what are and how do these function for each?

3.2. what technology is in hearing tech?
FM Systems
Infrared systems
Induction loop systems
one to one communicators
text telephones
speech recognition programs
closed-captioned tv
note taking
Devices that make phones louder
louder answering machines
loud doorbells

what are and how do these function for each?

3.3. what technology is in speech communication tech?
AAC
Alerting devices
infrared systems
educationsal software
memory aids
speech-generating device
unaided communication systems
electronic fluency devices

what are and how do these function for each?

3.4. what technology is in mobility tech?
adaptice bikes
adaptive strollers
wheelchairs and leg braces
adaptive electric strollers
canes
crutches
electric scooters
grab bars
lift vans
orthotic devices
prosthetic devices
ramps
rollators
walkers

what are and how do these function for each?

3.5. what technology is in daily living tech?
these are all in the other categories

3.6. what technology is in environmental adaptations tech?
Environmental modifications (E-Mods) are physical adaptations to the home that can increase or maintain your ability to live at home with independence. These improvements will address any safety issues you have. Environmental modifications include, but are not limited to: ramps, lifts, hand rails and bathroom modifications (such as roll-in showers).

3.6.1. what safety issues can people have in their environment?
3.6.2. what are rails?
3.6.3. what are roll-in shower?

3.7. what technology is in vehicle modifications and transportation tech?
WAV
hand acceleration/braking control
spinner knobs
walking rails
indicator/horn switches
pedal extensions

what are and how do these function for each?

3.8. what computer related accessibility tech exists?
adaptive keyboards
alternative input devices
head pointers
single switch entry devices
foot switches
sip-and-puff switches
eye-tracking software
AAC
braille display
braille notetaker
dictation software
electronic magnifiers
OCR
screen magnification software
screen reader software
TTS

what are and how do these function for each?

sources:
1. https://www.seewritehear.com/learn/what-is-accessibility/
1.1. https://www.w3.org/WAI/fundamentals/accessibility-usability-inclusion/
1.1.1. https://www.w3.org/WAI/standards-guidelines/wcag/wcag3-intro/
1.1.2. https://www.ucop.edu/electronic-accessibility/standards-and-best-practices/description-of-wcag-2.0.html
1.1.3. https://www.w3.org/WAI/standards-guidelines/evaluation/
1.2. https://www.levelaccess.com/how-we-do-our-work-matters-accessibility-and-inclusive-design/
1.2.1. https://www.nngroup.com/articles/inclusive-design/
1.2.2. https://medium.com/swlh/my-favorite-resources-for-learning-inclusive-design-and-accessibility-b8f24d5a90df
1.2.3. https://www.google.com/search?channel=crow5&client=firefox-b-d&q=where+can+I+find+people+with+disabilities%3F
1.3. https://www.washington.edu/doit/what-difference-between-accessible-usable-and-universal-design
1.3.1. https://www.w3.org/WAI/fundamentals/foundations-course/
1.3.2. https://www.dol.gov/general/topic/disability/ada
1.3.4. https://www.reliance-foundry.com/blog/universal-design
2. https://www.accessconsultancy.ie/Main-benefits-improving-accessibility-how-accessibility-benefitssociety
2.1. https://www.rochester.edu/college/disability/faculty/common-disabilities.html
2.1.1. https://www.hopkinsmedicine.org/health/conditions-and-diseases/adhdadd
2.1.2. https://www.ldrfa.org/the-top-5-most-common-learning-disabilities-their-symptoms/
2.1.3. https://www.washington.edu/doit/mobility-impairments
2.1.4. https://www.disabilitybenefitscenter.org/blog/top-10-disabilities
2.1.5. https://www.nice.org.uk/guidance/cg123/ifp/chapter/common-mental-health-problems
2.1.6. https://www.mayoclinic.org/diseases-conditions/traumatic-brain-injury/symptoms-causes/syc-20378557
2.1.7. https://psychiatry.org/patients-families/ptsd/what-is-ptsd
2.1.8. https://en.wikipedia.org/wiki/Visual_impairment
2.1.9. https://www.cdc.gov/headsup/basics/concussion_symptoms.html
2.1.10. https://www.helpguide.org/articles/autism-learning-disabilities/autism-spectrum-disorders.htm
2.2. https://www.wilc.org/the-philosophy-of-independent-living/
2.3. https://www.turku.fi/en/health-and-social-services/special-services-disabled/services-children-disabilities-under-18-years
2.3.1. https://www.ruralhealthinfo.org/toolkits/transportation/1/types-of-transportation
2.3.2. https://udservices.org/services/personal-care-independence/accessible-home-modifications/
2.3.3. https://www.nidirect.gov.uk/articles/equipment-people-disabilities
3. https://mn.gov/admin/at/getting-started/understanding-at/types/
3.1. https://www.djo.org.in/articles/30/2/Assistive-Technology-for-People-with-Visual-Loss.html
3.2. https://www.asha.org/public/hearing/hearing-assistive-technology/
3.3. https://www.physio-pedia.com/Assistive_Technology:_Communication_Products
3.4. https://thegenesisfoundation.org/types-of-adaptive-mobility-devices-for-people-with-disabilities/
3.5. https://www.google.com/search?q=assistive+technology+for+daily+living
3.6. https://opwdd.ny.gov/types-services/environmental-modifications-e-mods
3.7. https://modifieddriving.com.au/vehicle-modifications-to-enable-people-to-drive-with-a-disability/
3.8. https://bighack.org/assistive-technology-devices-definitions-how-disabled-people-use-the-web/